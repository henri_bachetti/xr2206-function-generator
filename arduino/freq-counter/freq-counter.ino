
#include "LedControl.h"
// https://github.com/wayoda/LedControl/releases

#include "FreqCounter.h"
// http://interface.khm.de/index.php/lab/interfaces-advanced/arduino-frequency-counter-library/
// Counter input must be D5.

#define LED     13
//LedControl lc=LedControl(DIN,CLK,CS,1);
LedControl lc = LedControl(8, 6, 7, 1);

unsigned long frq;
int ledState;

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  lc.shutdown(0, false);
  lc.setIntensity(0, 4);
  lc.clearDisplay(0);
  delay(2000);
  test(0);
}

void loop() {
  FreqCounter::f_comp = 10;             // Cal Value - Calibrate with professional Freq Counter
  FreqCounter::start(1000);             // 1000 ms Gate Time for 1Hz resolution.

  ledState = !ledState;
  digitalWrite(LED, ledState);
  while (FreqCounter::f_ready == 0);    // Wait for counter to be ready

  frq = FreqCounter::f_freq;
  lc.clearDisplay(0);                   // Clear LED display.
  printNumber(0, frq);                  // Break number into individual digits for LED display.
}

void test(int addr)
{
  for (int col = 0 ; col < 8 ; col++) {
    for (int row = 0 ; row < 8 ; row++) {
      lc.setLed(addr, row, col, HIGH);
      delay(50);
    }
  }
  delay(2000);
}

void printNumber(int addr, long num) {
  byte c;
  int j;
  int d;
  num < 1000 ? d = 4 : d = countDigits(num);
  for (j = 0; j < d; j++) {
    c = num % 10;                      // Modulo division = remainder
    num /= 10;                         // Divide by 10 = next digit
    boolean dp = (j == 3);             // Add decimal point at 3rd digit.
    lc.setDigit(addr, j, c, dp);
  }
}

int countDigits(long num) {
  int c = 0;
  while (num) {
    c++;
    num /= 10;
  }
  return c;
}




