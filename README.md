# XR2206-FUNCTION-GENERATOR

The purpose of this page is to explain step by step the realization of a function generator based on an XR2206.

The generator uses the following components :

 * an XR2206
 * a TL082 
 * a LM293
 * a LM7805
 * a LM7812
 * a LM7912
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/09/xr2206-le-generateur-de-fonctions.html

